# 기초 아키텍처

## 구조
![architecture](/uploads/dd3b20b3c5afa6fcb52d2cf2fb24a697/architecture.png)

## 데이터 흐름
![data_flow](/uploads/dccb92166296cf865eccd79f0cb19ef4/data_flow.png)