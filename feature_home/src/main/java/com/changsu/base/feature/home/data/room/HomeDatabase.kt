package com.changsu.base.feature.home.data.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

/**
 * The Room database for this app
 */
@Database(entities = [], version = 2, exportSchema = false)
abstract class HomeDatabase : RoomDatabase() {

    companion object {

        private const val databaseName = "home-db"

        fun buildDatabase(context: Context): HomeDatabase {
            return Room.databaseBuilder(context, HomeDatabase::class.java, databaseName)
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}