package com.changsu.base.feature.home.data.repository

import com.changsu.base.feature.home.data.retrofit.service.HomeRetrofitService
import com.changsu.base.feature.home.domain.repository.HomeRepository

internal class HomeRepositoryImpl(
    private val homeRetrofitService: HomeRetrofitService
) : HomeRepository {

}
