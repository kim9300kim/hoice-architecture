package com.changsu.base.feature.home

import com.changsu.base.app.feature.KodeinModuleProvider
import com.changsu.base.feature.home.data.dataModule
import com.changsu.base.feature.home.domain.domainModule
import com.changsu.base.feature.home.presentation.presentationModule
import org.kodein.di.Kodein

internal const val MODULE_NAME = "Home"

object FeatureKodeinModule : KodeinModuleProvider {

    override val kodeinModule = Kodein.Module("${MODULE_NAME}Module") {
        import(presentationModule)
        import(domainModule)
        import(dataModule)
    }
}
