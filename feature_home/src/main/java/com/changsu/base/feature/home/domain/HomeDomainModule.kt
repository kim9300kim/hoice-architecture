package com.changsu.base.feature.home.domain

import com.changsu.base.feature.home.MODULE_NAME
import org.kodein.di.Kodein

internal val domainModule = Kodein.Module("${MODULE_NAME}DomainModule") { }
