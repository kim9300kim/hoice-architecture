package com.changsu.base.feature.home.presentation

import androidx.fragment.app.Fragment
import com.changsu.base.feature.home.MODULE_NAME
import com.changsu.base.feature.home.presentation.home.HomeViewModel
import com.changsu.base.library.base.di.KotlinViewModelProvider
import org.kodein.di.Kodein
import org.kodein.di.android.x.AndroidLifecycleScope
import org.kodein.di.generic.bind
import org.kodein.di.generic.scoped
import org.kodein.di.generic.singleton

internal val presentationModule = Kodein.Module("${MODULE_NAME}PresentationModule") {

    bind<HomeViewModel>() with scoped<Fragment>(AndroidLifecycleScope).singleton {
        KotlinViewModelProvider.of(context) {
            HomeViewModel()
        }
    }

}
