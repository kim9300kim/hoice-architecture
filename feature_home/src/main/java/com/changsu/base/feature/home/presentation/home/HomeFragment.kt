package com.changsu.base.feature.home.presentation.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.changsu.base.app.presentation.fragment.NavigationFragment
import com.changsu.base.feature.home.databinding.FragmentHomeBinding
import kotlinx.android.synthetic.main.fragment_home.*
import org.kodein.di.generic.instance
import timber.log.Timber

class HomeFragment : NavigationFragment() {

    private val viewModel: HomeViewModel by instance()

    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(
            inflater, container, false
        ).apply {
            lifecycleOwner = viewLifecycleOwner
        }.also {
            Timber.v("onCreateView ${javaClass.simpleName}")
        }

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        underConstructionAnimation.playAnimation()
    }
}
