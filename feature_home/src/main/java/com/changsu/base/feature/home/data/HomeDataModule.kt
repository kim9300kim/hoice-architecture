package com.changsu.base.feature.home.data

import com.changsu.base.feature.home.MODULE_NAME
import com.changsu.base.feature.home.data.repository.HomeRepositoryImpl
import com.changsu.base.feature.home.data.retrofit.service.HomeRetrofitService
import com.changsu.base.feature.home.domain.repository.HomeRepository
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit

internal val dataModule = Kodein.Module("${MODULE_NAME}DataModule") {

    bind<HomeRepository>() with singleton { HomeRepositoryImpl(instance()) }

    bind() from singleton { instance<Retrofit>().create(HomeRetrofitService::class.java) }

}
