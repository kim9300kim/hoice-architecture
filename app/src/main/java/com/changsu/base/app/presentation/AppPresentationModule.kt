package com.changsu.base.app.presentation

import org.kodein.di.Kodein

internal val presentationModule = Kodein.Module("AppPresentationModule") {
}
