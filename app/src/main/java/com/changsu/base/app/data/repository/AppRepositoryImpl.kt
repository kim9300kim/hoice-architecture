package com.changsu.base.app.data.repository

import com.changsu.base.app.data.retrofit.service.AppRetrofitService
import com.changsu.base.app.domain.repository.AppRepository

internal class AppRepositoryImpl(
    private val appRetrofitService: AppRetrofitService
): AppRepository {
}