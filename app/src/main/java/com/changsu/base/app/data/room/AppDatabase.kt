package com.changsu.base.app.data.room

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase

/**
 * The Room database for this app
 */
// @Database annotation must specify list of entities
//@Database(entities = [], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    companion object {

        private const val databaseName = "app-db"

        fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, databaseName)
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}