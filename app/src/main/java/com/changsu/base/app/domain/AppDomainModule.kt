package com.changsu.base.app.domain

import org.kodein.di.Kodein

internal val domainModule = Kodein.Module("appDomainModule") {
}
