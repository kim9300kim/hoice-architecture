package com.changsu.base.app.data

import com.changsu.base.app.data.repository.AppRepositoryImpl
import com.changsu.base.app.data.retrofit.service.AppRetrofitService
import com.changsu.base.app.domain.repository.AppRepository
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit

internal val dataModule = Kodein.Module("appDataModule") {

    bind<AppRepository>() with singleton { AppRepositoryImpl(instance()) }

    bind() from singleton { instance<Retrofit>().create(AppRetrofitService::class.java) }

}