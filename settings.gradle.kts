include(":feature_home")
include(":library_test_utils")
include(":library_base")
include(":app")
rootProject.buildFileName = "build.gradle.kts"
